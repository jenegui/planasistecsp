<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <?php $this->load->helper("url"); ?>
            <h1><strong>Bienvenido a la aplicacion de Plan de Asistencia T&eacute;cnica Salud P&uacute;blica</strong></h1>
            <p><strong>1. OBJETIVO</strong>
Establecer directrices para la planeación, implementación, monitoreo y evaluación de la asistencia técnica (AT) que debe ser realizada por los servidores públicos que hacen parte de la Subsecretaría de Salud Pública (SSP).</p>
<p><strong>2. ALCANCE</strong>
Este lineamiento establece los parámetros para la planeación, implementación, monitoreo y evaluación de la asistencia técnica realizada por los servidores públicos que hacen parte de la SSP en el marco de la promoción de la salud, la gestión del riesgo en salud y la gestión de la salud pública, con el fin de garantizar los niveles de eficacia, eficiencia y efectividad de las mismas. <a href="<?php echo site_url("res/lineamiento.pdf"); ?>"  target="_blank">Ver m&aacute;s...</a></p>
            <br/>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 fondo-pest1">

            <h1><strong>INICIAR SESIÓN</strong></h1>

            <br/>

            <div class="form-group">

                <form id="frmIngresar" name="frmIngresar" method="post" action="<?php echo site_url("login/validar"); ?>" accept-charset="utf-8">

                    <div class="form-group">
                        <input class="form-control" type="text" id="txtLogin" name="txtLogin" value="" maxlength="15" placeholder="Usuario *"/>
                    </div>

                    <div class="form-group">
                        <input class="form-control" type="password" id="txtPassword" name="txtPassword" value="" maxlength="15" placeholder="Clave *"/>
                    </div>


                    <div>
                        <?php
                        if ($this->session->userdata("error_login") == 1) {
                            echo "<p style = 'font-size:12 px ; color: red'> Login/Password Incorrectos</p>";
                        } else {
                            echo "&nbsp;";
                        }
                        ?>
                    </div>

                    <div>
                        <button id="btnIngresar" name="btnIngresar" value="Ingresar" class="btn btn-primary btn-xl text-uppercase" type="submit">Ingresar</button>
                        <br/><br/>
                    </div>


                </form>
            </div>


        </div>

    </div>

    

</div>