<?php
if ($controller == "miniencuesta") {
    $this->config->load("sitio1");
    $this->load->helper("url");
} else {
    $this->config->load("sitio");
    $this->load->helper("url");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html lang="es" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" href="<?php echo base_url("images/favicon.png"); ?>">
        <!--[if lte IE 9]><link rel="stylesheet" href="<?php echo base_url("css/ie.css"); ?>" type="text/css" media="screen" /><![endif]-->
            <link rel="stylesheet" href="<?php echo base_url("css/1140.css"); ?>" type="text/css" media="screen" />
            <link rel="stylesheet" href="<?php echo base_url("css/styles.css"); ?>" type="text/css" media="screen" />
            <link href="<?php echo base_url("assets/bootstrap/vendor/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
            <!-- MetisMenu CSS -->
            <link href="<?php echo base_url("assets/bootstrap/vendor/metisMenu/metisMenu.min.css"); ?>" rel="stylesheet">
            <!-- Custom CSS -->
            <link href="<?php echo base_url("assets/bootstrap/dist/css/sb-admin-2.css"); ?>" rel="stylesheet">
            <!-- Custom Fonts -->
            <link href="<?php echo base_url("assets/bootstrap/vendor/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
            <!-- DataTables CSS -->
            <link href="<?php echo base_url("assets/bootstrap/vendor/datatables-plugins/dataTables.bootstrap.css"); ?>" rel="stylesheet">
            <!-- DataTables Responsive CSS -->
            <link href="<?php echo base_url("assets/bootstrap/vendor/datatables-responsive/dataTables.responsive.css"); ?>" rel="stylesheet">
            <link rel="stylesheet" href="<?php echo base_url("css/select2.css"); ?>" type="text/css" media="screen" />
            <!-- jQuery -->
            <script src="<?php echo base_url("assets/bootstrap/vendor/jquery/jquery.min.js"); ?>"></script>
            <!-- jQuery validate-->
            <script type="text/javascript" src="<?php echo base_url("assets/js/general/general.js"); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url("assets/js/general/jquery.validate.js"); ?>"></script>

            <link rel="stylesheet" href="<?php echo base_url("css/reportDatatable/jquery-ui.css"); ?>" type="text/css">
                    
            <title><?php echo $this->config->item("title"); ?></title>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <?php $this->load->view("template/headernlog"); ?>

                <?php
                if (isset($menu) && ($menu != ''))
                    $this->load->view($menu);
                else
                    $this->load->view("template/menu");
                ?>

            </div>
        </div>
        <div class="container">
            <div class="row">		
                <div id="container2" class="last">
                    <div id="contenido">
                        <?php $this->load->view($view); ?>
                        
                            <script src="<?php echo base_url("assets/bootstrap/vendor/bootstrap/js/bootstrap.min.js"); ?>"></script>
                            <!-- Metis Menu Plugin JavaScript -->
                            <script src="<?php echo base_url("assets/bootstrap/vendor/metisMenu/metisMenu.min.js"); ?>"></script>
                            <!-- Custom Theme JavaScript -->
                            <script src="<?php echo base_url("assets/bootstrap/dist/js/sb-admin-2.js"); ?>"></script>
                            <!-- DataTables JavaScript -->
                            <script src="<?php echo base_url("assets/bootstrap/vendor/datatables/js/jquery.dataTables.min.js"); ?>"></script>
                            <script src="<?php echo base_url("assets/bootstrap/vendor/datatables-plugins/dataTables.bootstrap.min.js"); ?>"></script>
                            <script src="<?php echo base_url("assets/bootstrap/vendor/datatables-responsive/dataTables.responsive.js"); ?>"></script>
                            <script type="text/javascript" src="<?php echo base_url("assets/js/general/ready.js"); ?>"></script>
                            <script type="text/javascript" src="<?php echo base_url("js/general/select2.js"); ?>"></script>
                            <script type="text/javascript" src="<?php echo base_url("assets/js/validate/planeacion/planeacion.js"); ?>"></script>
                            <script type="text/javascript" src="<?php echo base_url("js/reportDatatable/jquery-ui.js"); ?>"></script>

                        <!--<script type="text/javascript"> 
                            
                                $('#example').DataTable( {
                                    language: {
                                        processing:     "Procesando...",
                                        search:         "Buscar:",
                                        lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                                        info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                                        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                                        infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                                        infoPostFix:    "",
                                        loadingRecords: "Cargando...",
                                        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                                        emptyTable:     "Aucune donnée disponible dans le tableau",
                                        paginate: {
                                            first:      "Primera",
                                            previous:   "Anterior",
                                            next:       "Siguiente",
                                            last:       "Última"
                                        },
                                        buttons: {
                                            print:      "Imprimir",
                                            copy:       "Copiar",
                                            copyTitle: "Copiar al portapapeles"
                                            
                                        },
                                        aria: {
                                            sortAscending:  ": activer pour trier la colonne par ordre croissant",
                                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                                        }
                                    },
                                    dom: 'Bfrtip',
                                    buttons: [
                                        'copy', 'csv', 'excel'
                                    ],
                                    sPaginationType: "full_numbers",
                                    aaSorting: [[0, "asc" ]],
                                    bPaginate: true,
                                    bLengthChange: true,
                                    bFilter: true,
                                    bSort: true,
                                    bInfo: true,
                                    //bJQueryUI: true,
                                    //bAutoWidth: true, 
                                    processing: true,
                                    //serverSide: true,
                                    responsive: true,
                                    bProcessing: true,
                                } );
                            
                        </script>     -->     
                    </div>
                </div>
            </div>
        </div>  



        <div class="container">
            <div id="footer" class="row">
                <div id="textlogo">			
                    <?php $this->load->view("template/footer"); ?>
                </div>
            </div>
        </div>
    			
    </body>
</html> 