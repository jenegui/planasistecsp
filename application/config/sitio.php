<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Configura el texto para el title del sitio
$config['title']	= "Plan de Asistencia Técnica Salud Pública";

//Configura el nombre del sitio
$config['header']	= "Plan de Asistencia Técnica <br> Salud Pública";

//Texto para el footer del sitio
$config['footer']   = "2020. @ Todos los derechos reservados<br>*Habeas data<br>*Términos y condiciones<br>";


