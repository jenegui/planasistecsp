<script type="text/javascript" src="<?php echo base_url("assets/js/validate/planeacion/cronograma.js"); ?>"></script>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="exampleModalLabel">Registro cronograma mes <?php echo $mes;?>
		<br><small>Adicionar</small>
	</h4>
</div>

<div class="modal-body">

	<p class="text-danger text-left">Los campos con * son obligatorios.</p>

	<form name="form" id="form" role="form" method="post" >
		<input type="hidden" id="hddId" name="hddId" value="<?php echo $idActividad;?>"/>
		<input type="hidden" id="mes" name="mes" value="<?php echo $information?$information[0]["mes"]:$mes;?>"/>
		<input type="hidden" id="idCronograma" name="idCronograma" value="<?php echo $information?$information[0]["idCronograma"]:"x"; ?>"/>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="fechaEjecucion">Fecha de ejecuci&oacute;n: *</label>
					<input type="text" id="fechaEjecucion<?php echo $mes;?>" name="fechaEjecucion" class=" form-control datepicker" value="<?php echo $information?$information[0]["fecha_ejecucion"]:""; ?>" placeholder="mm/dd/aaaa" required>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="numAsistentes">N&uacute;mero de asistentes : *</label>
					<input type="text" id="numAsistentes" name="numAsistentes" class=" form-control" value="<?php echo $information?$information[0]["nro_asistentes"]:""; ?>" placeholder="N&uacute;mero de asistentes" required>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="personas">Facilitadores:</label>
					<textarea name="facilitadores" id="facilitadores" class="form-control" rows="3" cols="50" required><?php echo $information?$information[0]["facilitadores"]:"";?></textarea>
				</div>		
			</div>	
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="soportes">Soportes: *</label>
					<input type="text" id="soportes" name="soportes" class=" form-control" value="<?php echo $information?$information[0]["soportes"]:""; ?>" placeholder="Ruta de almacenamiento" required>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="personas">Observaciones:</label>
					<textarea name="observaciones" id="observaciones" class="form-control" rows="3" cols="50" required><?php echo $information?$information[0]["observaciones"]:"";?></textarea>
				</div>		
			</div>	
		</div>
		<div class="form-group">
			<div class="row" align="center">
				<div style="width:50%;" align="center">
					<input type="button" id="btnSubmit" name="btnSubmit" value="Guardar" class="btn btn-primary"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div id="div_load" style="display:none">		
				<div class="progress progress-striped active">
					<div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
						<span class="sr-only">45% completado</span>
					</div>
				</div>
			</div>
			<div id="div_error" style="display:none">			
				<div class="alert alert-danger"><span class="glyphicon glyphicon-remove" id="span_msj">Ya existe un registro con los datos ingresados.</span></div>
			</div>	
		</div>

	</form>
</div>
