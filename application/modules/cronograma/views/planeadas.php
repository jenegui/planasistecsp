<div id="page-wrapper">
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="list-group-item-heading">
						<i class="fa fa-gear fa-fw"></i> &nbsp; SECCI&Oacute;N 1. PLANEACI&Oacute;N
					</h4>
				</div>
			</div>
		</div>
		<!-- /.col-lg-12 -->				
	</div>
	
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-reorder"></i> LISTA DE ACTIVDADES PLANEADAS
				</div>
				<div class="panel-body">
					<button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal" id="x">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Adicionar Actividad
					</button><br>
					<?php
					$retornoExito = $this->session->flashdata('retornoExito');
					if ($retornoExito) {
						?>
						<div class="col-lg-12">	
							<div class="alert alert-success ">
								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								<?php echo $retornoExito ?>		
							</div>
						</div>
						<?php
					}

					$retornoError = $this->session->flashdata('retornoError');
					if ($retornoError) {
						?>
						<div class="col-lg-12">	
							<div class="alert alert-danger ">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<?php echo $retornoError ?>
							</div>
						</div>
						<?php
					}
					?> 
					<?php
					if($info){
						?>				
						<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
							<thead>
								<tr>
									<th class="text-center">No. Actividad</th>
									<th class="text-center">Editar</th>
									<th class="text-center">Modalidad Asistencia T&eacute;cnica</th>
									<th class="text-center">Ambito de aplicaci&oacute;n</th>
									<th class="text-center">&Aacute;rea de operaci&oacute;n</th>
									<th class="text-center">Otra area de operaci&oacute;n</th>
									<th class="text-center">N&uacute;mero de personas </th>
									<th class="text-center">Cronograma de planeación y seguimiento a la implementaci&oacute;n </th>
									<th class="text-center">Tipo de capacidad a desarrollar</th>								
									<th class="text-center">Tema de la Asistencia T&eacute;cnica</th>
									<th class="text-center">Dependencia(s) responsable(s)</th>
									<th class="text-center">Meses</th>
									<th class="text-center">Fecha de registro</th>
									<th class="text-center">Estado</th>
								</tr>
							</thead>
							<tbody>							
								<?php
								foreach ($info as $lista):
									$idActividad=$lista['idActividad'];
									echo "<tr>";
									echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;" . $lista['idActividad'] . "</td>";
									echo "<td class='text-center'>";
									?>
									<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal" id="<?php echo $lista['idActividad']; ?>" >
										Editar <span class="glyphicon glyphicon-edit" aria-hidden="true">
										</button>
										<?php
										echo "</td>";
										echo "<td class='text-center'>" . $lista['nomModalidad'] . "</td>";
										echo "<td class='text-center'>". $lista['nomAmbito'] ."</td>";
										echo "<td class='text-center'>". $lista['nomArea'] ."</td>";
										if($lista['cual'] != '0'){
											echo "<td class='text-center'>" . $lista['cual'] ."</td>";
										}else{
											echo "<td class='text-center'> </td>";
										}
										echo "<td class='text-center'>" . $lista['num_personas'] . "</td>";
										
										echo "<td class='text-center'>";
										?>
									<!-- 
										Se quita la opcion de resetear la contraseña a 123456
									<a href="<?php echo base_url("admin/resetPassword/$idActividad" . $lista['idUsuario']); ?>" class="btn btn-default btn-xs">Reset <span class="glyphicon glyphicon-lock" aria-hidden="true"></a> 
									-->
									<a href="<?php echo base_url("planeacion/cronograma/" . $lista['idActividad']); ?>" class="btn btn-default btn-xs">Cronograma y seguimeinto <span class="glyphicon glyphicon-edit" aria-hidden="true"></a>

										<?php
										echo "</td>";
										echo "<td class='text-center'>" . $lista['nombreTipoCapacidad'] . "</td>";
										echo "<td class='text-center'>" . $lista['tema'] . "</td>";
										echo "<td class='text-center'>";
										echo '<p class="text-primary">'; 
										for($i=1; $i<=6; $i++){
											$j=$i-1; 
											//Rescata la lista de dependencias
											if($lista['dependencia_'.$i]==1){
										 		echo "- " .$dependencia[$j]['nomDependencia']."<br>";
										 	}
										}
										echo "</p></td>";
										echo "<td class='text-center'>";
										echo '<p class="text-primary">';
										for($i=1; $i<=12; $i++){
											$j=$i-1; 
											//Rescata la lista de dependencias
											if($lista['mes_'.$i]==1){
										 		echo "- " .$mes[$j]['nomMes']."<br>";
										 	}
										}
										echo "<td class='text-center'>" . $lista['fechaRegistro'] . "</td>";
										echo "<td class='text-center'>";
										switch ($lista['estado_actividad']) {
											case 0:
											$valor = 'Inactivo';
											$clase = "text-danger";
											break;
											case 1:
											$valor = 'Activo';
											$clase = "text-success";
											break;
											case 2:
											$valor = 'Inactivo';
											$clase = "text-primary";
											break;
										}
										echo '<p class="' . $clase . '"><strong>' . $valor . '</strong></p>';
										echo "</td>";
										echo "</tr>";
									endforeach;
									?>
								</tbody>
							</table>
						<?php } ?>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->


	<!--INICIO Modal para adicionar HAZARDS -->
	<div class="modal fade text-center" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">    
		<div class="modal-dialog" role="document">
			<div class="modal-content" id="tablaDatos">

			</div>
		</div>
	</div>                       
	<!--FIN Modal para adicionar HAZARDS -->

	<!-- Tables -->
	