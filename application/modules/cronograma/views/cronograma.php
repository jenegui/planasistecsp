<?php 
	//Los datos que yo recibo en esta vista se propagan a traves de las demas vistas qye yo llame desde esta vista, 
	//por lo que la variable controller puede ser accedida desde cualquiera de las vistas de los capitulos.
	$this->load->library("general");
	
?>
<div id="content">
	<div class="panel-heading">
		<a class="btn btn-success" href=" <?php echo base_url(). 'planeacion/planeada'; ?> "><span class="glyphicon glyphicon glyphicon glyphicon-backward" aria-hidden="true"></span> Regresar </a> 
	</div>
	<div id="tabs">
	  	<ul>
	  		<?php
	  		foreach ($info as $lista):
	  			for($i=1; $i<=12; $i++){
	  				$j=$i-1; 
					//Rescata la lista de meses
	  				if($lista['mes_'.$i]==1){
	  					$mes=$meses[$j]['nomMes'];
	  					echo '<li><a href="#tabs-'. $i .'">'. $meses[$j]['nomMes'] .'<img id="imgtab'. $i .'" src="'. $this->general->obtenerImagen(1).'" border="0" style="padding-left: 10px;"/></a></li>';
	  				}
	  			}
	  		endforeach;
	  		?>
	  	</ul>
	  	<?php
  		foreach ($info as $lista):
  			for($i=1; $i<=12; $i++){
  				$j=$i-1;
  				if($lista['mes_'.$i]>=1){ 
  					echo '<div id="tabs-'.$i.'">';
  						switch ($i) {
  							case $i:
  							 	# code...
  								
  								//echo '<input type="hidden" id="nro_establecimiento" name="nro_establecimiento" value="'.$i.'"/>';
  								?>
								<div id="page-wrapper">
									<br>
									<div class="row">
										<div class="col-md-12">
											<div class="panel panel-primary">
												<div class="panel-heading">
													<h4 class="list-group-item-heading">
														<i class="fa fa-gear fa-fw"></i> &nbsp; SECCIÓN  2. CRONOGRAMA DE PLANEACIÓN  y SEGUIMIENTO A LA IMPLEMENTACIÓN
													</h4>
												</div>
											</div>
										</div>
										<!-- /.col-lg-12 -->				
									</div>									
									<!-- /.row -->
									<div class="row">
										<div class="col-lg-12">
											<div class="panel panel-default">
												<div class="panel-heading">
													<i class="fa fa-reorder"></i> LISTA DE ACTIVDADES PLANEADAS <?php echo strtoupper($meses[$j]['nomMes']) ; ?>
												</div>
												<div class="panel-body">
													<button type="button" class="btn btn-success btn-block <?php echo 'mes'.$i; ?>" data-toggle="modal" data-target="#modal<?php echo $i; ?>" id="<?php echo $i; ?>">
														<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Adicionar Actividad</button><br>
													<?php
													$retornoExito = $this->session->flashdata('retornoExito');
													if ($retornoExito) {
														?>
														<div class="col-lg-12">	
															<div class="alert alert-success ">
																<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
																<?php echo $retornoExito ?>		
															</div>
														</div>
														<?php
													}

													$retornoError = $this->session->flashdata('retornoError');
													if ($retornoError) {
														?>
														<div class="col-lg-12">	
															<div class="alert alert-danger ">
																<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
																<?php echo $retornoError ?>
															</div>
														</div>
														<?php
													}
													?> 
													<?php
													if($info){
														?>				
														<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables<?php echo $i; ?>">
															<thead>
																<tr>
																	<th class="text-center">No. Actividad</th>
																	<th class="text-center">Editar</th>
																	<th class="text-center">Mes</th>
																	<th class="text-center">fecha_ejecuci&oacute;n</th>
																	<th class="text-center">Nro. asistentes </th>
																	<th class="text-center">Facilitadores</th>
																	<th class="text-center">Soportes</th>
																	<th class="text-center">Observaciones </th>
																	<th class="text-center">Estado</th>
																</tr>
															</thead>
															<tbody>							
																<?php
																foreach ($infoCron as $listaCron):
																	if($listaCron['mes']==$i && $idActividad==$listaCron['id_actividad']){
																		$regVacio="";
																		echo "<tr>";
																			echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;" . $idActividad . "</td>";
																			echo "<td class='text-center'>";
																			?>
																			<button type="button" class="btn btn-success btn-xs <?php echo 'mes'.$i; ?>" data-toggle="modal" data-target="#modal<?php echo $i; ?>" id="Ed-<?php echo $listaCron['idCronograma']; ?>">
																				Editar <span class="glyphicon glyphicon-edit" aria-hidden="true">
																				</button>
																				<?php
																			
																				echo "</td>";
																				echo "<td class='text-center'>" . $listaCron['mes'] . "</td>";
																				echo "<td class='text-center'>". $listaCron['fecha_ejecucion'] ."</td>";
																				echo "<td class='text-center'>". $listaCron['nro_asistentes'] ."</td>";
																				echo "<td class='text-center'>" . $listaCron['facilitadores'] . "</td>";
																				echo "<td class='text-center'>" . $listaCron['soportes'] . "</td>";
																				echo "<td class='text-center'>" . $listaCron['observaciones'] . "</td>";
																				echo "<td class='text-center'>" . $listaCron['estado'] . "</td>";
																		}else{
																			$regVacio= "";
																		
																		echo "</tr>";
																	}
																endforeach;
																
																?>
															</tbody>
														</table>
													<?php } ?>
												</div>
												<!-- /.panel-body -->
											</div>
											<!-- /.panel -->
										</div>
										<!-- /.col-lg-12 -->
									</div>
									<!-- /.row -->
								</div>
									<!-- /#page-wrapper -->


								<!--INICIO Modal para adicionar HAZARDS -->
								<div class="modal fade text-center" id="modal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">    
									<div class="modal-dialog" role="document">
										<div class="modal-content" id="tablaDatos<?php echo $i; ?>">

										</div>
									</div>
								</div>                       
								<!--FIN Modal para adicionar HAZARDS -->

								<!-- Tables -->	
								<?php
  								break;
  							default:
  								# code...
  								break;
  						}
				    	//'.$this->load->view($meses[$j]['nomMes']).'
				  	echo '</div>';
				}
  			}
  		endforeach;
  		?>
  	</div>
</div>
		
<!--<li><a href="#tabs-2">Proin dolor</a></li>
				    <li><a href="#tabs-3">Aenean lacinia</a></li>

				 //'. $this->general->obtenerImagen($modulo1["imagen"]).'-->
