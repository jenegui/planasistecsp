<fieldset style="border: 1px solid #CCCCCC; padding: 10px;">
    <legend><b>&nbsp; SECCI&Oacute;N 1. PLANEACIÓN</b></legend>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                <div class="form-group">
                    <form id="frmIngresar" name="frmIngresar" method="post" action="<?php echo site_url("login/validar"); ?>" accept-charset="utf-8">
                    <div class="form-group">
                        <input class="form-control" type="text" id="txtLogin" name="txtLogin" value="" maxlength="15" placeholder="No. de actividad *"/>    
                    </div>
                    <div class="form-group">
                         
                            <label for="">Modalidad de asistencia técnica:</label>
                        
                            <select id="modalidad" name="modalidad" style="width:250px;" class="select">
                                <option value="-">Seleccione...</option>
                                <?php
                                for ($i = 0; $i < count($modalidad); $i++) {
                                   //if ($modalidad[$i]["id_modalidad"] == $id_usuario) { 
                                ?>
                                    <option value="<?php echo $modalidad[$i]["id_modalidad"]; ?>"><?php echo $modalidad[$i]["modalidad"]; ?></option> 
                                <?php
                                    //}
                                }
                                ?>
                            </select>
                       
                    </div>
                    <div class="form-group">
                         
                            <label for="">Ámbito de aplicación:</label>
                        
                            <select id="ambito" name="ambito" style="width:250px;" class="select">
                                <option value="-">Seleccione...</option>
                                <?php
                                for ($i = 0; $i < count($ambito); $i++) {
                                   //if ($ambito[$i]["id_modalidad"] == $id_usuario) { 
                                ?>
                                    <option value="<?php echo $ambito[$i]["id_ambito"]; ?>"><?php echo $ambito[$i]["ambito"]; ?></option> 
                                <?php
                                    //}
                                }
                                ?>
                            </select>
                       
                    </div>
                    <div class="form-group">
                         
                            <label for="">Área de operación:</label>
                        
                            <select id="area" name="area" style="width:250px;" class="select">
                                <option value="-">Seleccione...</option>
                                <?php
                                for ($i = 0; $i < count($area); $i++) {
                                   //if ($area[$i]["id_modalidad"] == $id_usuario) { 
                                ?>
                                    <option value="<?php echo $area[$i]["id_area"]; ?>"><?php echo $area[$i]["area"]; ?></option> 
                                <?php
                                    //}
                                }
                                ?>
                            </select>
                       
                    </div>
                    <div class="form-group otro" style= "display:none">
                        <input class="form-control" type="text" id="otroCual" name="otroCual" value="" maxlength="15" placeholder="Otro cuál? *"/>    
                    </div>
                    <div class="form-group">
                        <label for="">Número de personas:</label>
                        <input class="form-control" type="text" id="numpersonas" name="numpersonas" value="" maxlength="15" placeholder="No. de personas *"/>    
                    </div>
                    <div class="form-group">
                         
                            <label for="">Tipo de capacidad a desarrollar:</label>
                        
                            <select id="capacidad" name="capacidad" style="width:250px;" class="select">
                                <option value="-">Seleccione...</option>
                                <?php
                                for ($i = 0; $i < count($capacidad); $i++) {
                                   //if ($capacidad[$i]["id_modalidad"] == $id_usuario) { 
                                ?>
                                    <option value="<?php echo $capacidad[$i]["id_tipo_capacidad"]; ?>"><?php echo $capacidad[$i]["tipoCapacidad"]; ?></option> 
                                <?php
                                    //}
                                }
                                ?>
                            </select>
                       
                    </div>
                    <div class="form-group">
                        <!-- Default unchecked -->
                    <div class="form-check"">
                        <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
                        <label class="custom-control-label" for="defaultUnchecked">Default unchecked</label>
                    </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <button id="btnIngresar" name="btnIngresar" value="Ingresar" class="btn btn-primary btn-xl text-uppercase" type="submit">Registrar</button>
                        </div>
                    </div>
                    </form>
                </div>        
            </div>
        </div>
    </div>    
</fieldset>
