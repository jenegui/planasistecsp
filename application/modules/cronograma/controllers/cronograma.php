<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cronograma extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->library("validarsesion");
        $this->load->library("general");
    }

    
    public function index($idActividad) {
        if($idActividad==0){
            $idActividad=$this->session->userdata("idActividad");
        }else{
            $idActividad=$idActividad;
        }
        $this->load->model("planeadas");
        $this->load->model("parametricas");
        $data["controller"] = $this->session->userdata("controlador");
        $nom_usuario = $this->session->userdata("nombre");
        $tipo_usuario = $this->session->userdata("tipo_usuario");
        $data['tipo_usuario'] = $this->session->userdata("tipo_usuario");
        $data["nom_usuario"] = $nom_usuario;
        $this->session->set_userdata("id_actividad",$idActividad);
        
        $data["idActividad"] = $idActividad;
        $arrParam = array(
                "idActividad" => $idActividad
            );
        $data['meses'] = $this->parametricas->obtenerMeses();
        $data['info'] = $this->planeadas->get_actividades($arrParam);
        $data['infoCron'] = $this->planeadas->get_cronograma($arrParam);
        $data["view"] = "cronograma";
        $data["menu"] = "planeacionmenu";
        //$data["periodos"] = $this->periodo->obtenerPeriodosTodosMOD();
        
        $this->load->view("layout", $data);
    }

    //Método para el primer formualario que corresponde a la sección 1 PLANEACIÓN
    public function planeada(){
        $this->load->model("planeadas");
        $this->load->model("parametricas"); 
        $data["controller"] = $this->session->userdata("controlador");
        $arrParam = array();
        $data['info'] = $this->planeadas->get_actividades($arrParam);
        $data["view"] = "planeadas";
        $data["menu"] = "planeacionmenu";
        $data["modalidad"] = $this->parametricas->obtenerModalidad();
        $data["ambito"] = $this->parametricas->obtenerAmbito();
        $data["area"] = $this->parametricas->obtenerArea(0);
        $data["capacidad"] = $this->parametricas->obtenerCapacidad();
        $data["dependencia"] = $this->parametricas->obtenerDependencia();
        $data["mes"] = $this->parametricas->obtenerMeses();
        $this->load->view("layout", $data);
    }

    /**
     * Cargo modal - formulario Usuarios
     * @since 21/01/2020
     */
    public function cargarModal() 
    {
        header("Content-Type: text/plain; charset=utf-8"); //Para evitar problemas de acentos
        $this->load->model("planeadas");
        $this->load->model("parametricas");
        $data['information'] = FALSE;

        $mystring =  $this->input->post("idRow");
        $findme   = 'Ed-';
        $pos = strpos($mystring, $findme);
        if ($pos === false) {
            $data["mes"] = $this->input->post("idRow");
            $data["idCronograma"]='x';
            //echo "La cadena ".$findme." no fue encontrada en la cadena ".$mystring;
        } else {
            $data["mes"] ='';
            $idCronogrma=$this->input->post("idRow");
            $resultado = substr($idCronogrma, 3);
            $data["idCronograma"] = $resultado;
           //echo "La cadena ".$findme." fue encontrada en la cadena ".$mystring;
            //echo " y existe en la posición ".$pos;
        }

        
        $data["menu"] = ("administrador/adminmenu");
        //$this->load->model("general_model");
        $arrParam = array(
            "table" => "pat_param_roles",
            "order" => "nom_rol",
            "id" => "x"
        );
        
        $data['idActividad']=$this->session->userdata("id_actividad");
        
        $data['roles'] = $this->planeadas->get_basic_search($arrParam);
        $data["modalidad"] = $this->parametricas->obtenerModalidad();
        $data["ambito"] = $this->parametricas->obtenerAmbito();
        $data["area"] = $this->parametricas->obtenerArea(0);
        $data["capacidad"] = $this->parametricas->obtenerCapacidad();         
        $data['dependencia'] = $this->parametricas->obtenerDependencia();
        $data['meses'] = $this->parametricas->obtenerMeses(); 

        if ($data["idCronograma"] != 'x') 
        {
            $arrParam = array(
                "idCronograma" => $data["idCronograma"]
            );
            $data['information'] = $this->planeadas->get_cronograma($arrParam);
        }
        
        $this->load->view("cronograma_modal", $data);
    }

    //Actualiza un combo de Municipios con base en un combo de departamentos
    public function actualizararea() {
        $this->load->model("parametricas");
        $idambito = $this->input->post("id");
        $areas = $this->parametricas->obtenerArea($idambito);
        echo '<option value="-" selected="selected">Seleccione</option>';
        for ($i = 0; $i < count($areas); $i++) {
            echo '<option value="' . $areas[$i]["id_area"] . '">' . $areas[$i]["area"] . '</option>';
        }
    }

    ///Método para registrar cronograma y seguimiento
    public function planeacion($idActividad) {
        $this->load->model("planeadas");
        $this->load->model("parametricas");
        $data["controller"] = $this->session->userdata("controlador");
        $nom_usuario = $this->session->userdata("nombre");
        $tipo_usuario = $this->session->userdata("tipo_usuario");
        $data['tipo_usuario'] = $this->session->userdata("tipo_usuario");
        $data["nom_usuario"] = $nom_usuario;
        $data["idActividad"] = $idActividad;
        $arrParam = array(
                "idActividad" => $idActividad
            );
        $data['meses'] = $this->parametricas->obtenerMeses();
        $data['info'] = $this->planeadas->get_actividades($arrParam);
        $data["view"] = "cronograma";
        $data["menu"] = "planeacionmenu";
        //$data["periodos"] = $this->periodo->obtenerPeriodosTodosMOD();
        
        $this->load->view("layout", $data);
    }


    /**
     * Actualiza actividad
     * @since 21/01/2020
     
     */
    public function save_cronograma()
    {           
        header('Content-Type: application/json');
        $this->load->library("general");
        $this->load->model("planeadas");
        $data = array();
        $idUsuario = $this->session->userdata("id");
        $idCronograma = $this->input->post('idCronograma');
        $idActividad = $this->input->post('hddId');
        $this->session->set_userdata('idActividad', $idActividad);
        $msj = "Se adicionó una nueva actividad.";
        if ($idCronograma != '') {
            $msj = "Se actualizó la actividad exito.";
        }           

        $documento = $this->input->post('documento');
        
        $result_cronograma = false;
        $clave = "";
        if ($idCronograma == 'x') {
            //Verify if the user already exist by the user name
            foreach ($_POST as $nombre_campo => $valor) {
                if(!isset($valor)){
                    $valor1=0;
                }else{
                    $valor1=$valor;
                }
                $asignacion = "\$" . $nombre_campo . "='" . $valor1 . "';";
                eval($asignacion);
             }
            $fecha= $this->general->formatoFecha($fechaEjecucion,'/');    
            $result_cronograma = $this->planeadas->verifyCronograma($idActividad, $fecha, $numAsistentes, $facilitadores, $soportes, $observaciones);

            //$pass = $this->generar_clave();
            $clave = "";
            //$clave = $this->danecrypt->encode($pass);
         }

        if ($result_cronograma) {
            $data["result"] = "error";
            $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Ya existe un registro con los valores digitados.');
        } else {
                if ($idCronogrma = $this->planeadas->saveCronograma($clave)) {
                    $data["result"] = true;                 
                    $this->session->set_flashdata('retornoExito', $msj);
                    
                    //a los usuarios nuevos les envio correo con contraseña
                    /*if($idUser == '') {
                        $this->email($idUsuario);
                    }*/
                } else {
                    $data["result"] = "error";                  
                    $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Contactarse con el administrador.');
                }
        }

        echo json_encode($data);
    }
    
    public function cerrarSesion() {
        $this->load->helper("url");
        $this->load->library("session");
        $this->session->sess_destroy();
        redirect("login", "refresh");
    }

}

//EOC