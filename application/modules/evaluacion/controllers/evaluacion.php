<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Evaluacion extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->library("validarsesion");
        $this->load->library("general");
    }

    
    public function evaluar($idActividad) {
        if($idActividad==0){
            $idActividad=$this->session->userdata("idActividad");
        }else{
            $actividad=$idActividad;
            $this->session->set_userdata("idActividad",$actividad);

        }
        $this->load->model("planeadas");
        $this->load->model("parametricas");
        $data["controller"] = $this->session->userdata("controlador");
        $nom_usuario = $this->session->userdata("nombre");
        $tipo_usuario = $this->session->userdata("tipo_usuario");
        $data['tipo_usuario'] = $this->session->userdata("tipo_usuario");
        $data["nom_usuario"] = $nom_usuario;
        $idActividad=$this->session->userdata("idActividad");
        
        $data["idActividad"] = $idActividad;
        $arrParam = array(
                "idActividad" => $idActividad
            );
        $data['meses'] = $this->parametricas->obtenerMeses();
        $data['info'] = $this->planeadas->get_actividades($arrParam);
        $data['infoEval'] = $this->planeadas->get_evaluaciones($arrParam);
        $data["view"] = "evaluacion";
        $data["menu"] = "evaluacionmenu";
        //$data["periodos"] = $this->periodo->obtenerPeriodosTodosMOD();
        
        $this->load->view("layout", $data);
    }

    //Método para el primer formualario que corresponde a la sección 1 PLANEACIÓN
    public function planeada(){
        $this->load->model("planeadas");
        $this->load->model("parametricas"); 
        $data["controller"] = $this->session->userdata("controlador");
        $arrParam = array();
        $data['info'] = $this->planeadas->get_actividades($arrParam);
        $data["view"] = "planeadas";
        $data["menu"] = "planeacionmenu";
        $data["modalidad"] = $this->parametricas->obtenerModalidad();
        $data["ambito"] = $this->parametricas->obtenerAmbito();
        $data["area"] = $this->parametricas->obtenerArea(0);
        $data["capacidad"] = $this->parametricas->obtenerCapacidad();
        $data["dependencia"] = $this->parametricas->obtenerDependencia();
        $data["mes"] = $this->parametricas->obtenerMeses();
        $this->load->view("layout", $data);
    }

    /**
     * Cargo modal - formulario Usuarios
     * @since 21/01/2020
     */
    public function cargarModal() 
    {
        header("Content-Type: text/plain; charset=utf-8"); //Para evitar problemas de acentos
        $this->load->model("planeadas");
        $this->load->model("parametricas");
        $data['information'] = FALSE;
        $data["idEvaluacion"] = $this->input->post("idRow");

        $mystring =  $this->input->post("idRow");
        echo $mystring;
        $findme   = 'Ed-';
        $pos = strpos($mystring, $findme);
        if ($pos === false) {
            $data["mes"] = $this->input->post("idRow");
            $data["idEvaluacion"]='x';
            //echo "La cadena ".$findme." no fue encontrada en la cadena ".$mystring;
        } else {
            $data["mes"] ='';
            $idCronogrma=$this->input->post("idRow");
            $resultado = substr($idCronogrma, 3);
            $data["idEvaluacion"] = $resultado;
           //echo "La cadena ".$findme." fue encontrada en la cadena ".$mystring;
            //echo " y existe en la posición ".$pos;
        }
        
        $data["menu"] = ("administrador/adminmenu");
        //$this->load->model("general_model");
        $arrParam = array(
            "table" => "pat_param_roles",
            "order" => "nom_rol",
            "id" => "x"
        );
        
        $data['idActividad']=$this->session->userdata("id_actividad");
        //$data["idCronograma"]='x';
        $data['roles'] = $this->planeadas->get_basic_search($arrParam);
        $data["modalidad"] = $this->parametricas->obtenerModalidad();
        $data["ambito"] = $this->parametricas->obtenerAmbito();
        $data["area"] = $this->parametricas->obtenerArea(0);
        $data["capacidad"] = $this->parametricas->obtenerCapacidad();         
        $data['dependencia'] = $this->parametricas->obtenerDependencia();
        $data['meses'] = $this->parametricas->obtenerMeses(); 

        if ($data["idEvaluacion"] != 'x') 
        {
            $arrParam = array(
                "idEvaluacion" => $data["idEvaluacion"]
            );
            $data['information'] = $this->planeadas->get_evaluaciones($arrParam);
        }
        
        $this->load->view("evaluacion_modal", $data);
    }

    //Actualiza un combo de Municipios con base en un combo de departamentos
    public function actualizararea() {
        $this->load->model("parametricas");
        $idambito = $this->input->post("id");
        $areas = $this->parametricas->obtenerArea($idambito);
        echo '<option value="-" selected="selected">Seleccione</option>';
        for ($i = 0; $i < count($areas); $i++) {
            echo '<option value="' . $areas[$i]["id_area"] . '">' . $areas[$i]["area"] . '</option>';
        }
    }

    ///Método para registrar cronograma y seguimiento
    public function planeacion($idActividad) {
        $this->load->model("planeadas");
        $this->load->model("parametricas");
        $data["controller"] = $this->session->userdata("controlador");
        $nom_usuario = $this->session->userdata("nombre");
        $tipo_usuario = $this->session->userdata("tipo_usuario");
        $data['tipo_usuario'] = $this->session->userdata("tipo_usuario");
        $data["nom_usuario"] = $nom_usuario;
        $data["idActividad"] = $idActividad;
        $arrParam = array(
                "idActividad" => $idActividad
            );
        $data['meses'] = $this->parametricas->obtenerMeses();
        $data['info'] = $this->planeadas->get_actividades($arrParam);
        $data["view"] = "cronograma";
        $data["menu"] = "planeacionmenu";
        //$data["periodos"] = $this->periodo->obtenerPeriodosTodosMOD();
        
        $this->load->view("layout", $data);
    }


    /**
     * Actualiza actividad
     * @since 21/01/2020
     
     */
    public function save_evaluacion()
    {           
        header('Content-Type: application/json');
        $this->load->library("general");
        $this->load->model("planeadas");
        $data = array();
        $idUsuario = $this->session->userdata("id");
        $idEvaluacion = $this->input->post('idEvaluacion');
        $idActividad = $this->input->post('hddId');
        $this->session->set_userdata('idActividad', $idActividad);
        $msj = "Se adicionó una nueva actividad.";
        if ($idEvaluacion != '') {
            $msj = "Se actualizó la actividad exito.";
        }           

        $documento = $this->input->post('documento');
        
        $result_evaluacion = false;
        $clave = "";
        if ($idEvaluacion == 'x') {
            //Verify if the user already exist by the user name
            foreach ($_POST as $nombre_campo => $valor) {
                if(!isset($valor)){
                    $valor1=0;
                }else{
                    $valor1=$valor;
                }
                $asignacion = "\$" . $nombre_campo . "='" . $valor1 . "';";
                eval($asignacion);
             }
            //$fecha= $this->general->formatoFecha($fechaEvaluacion,'/');    
            $result_evaluacion = $this->planeadas->verifyEvaluacion($idActividad, $evaluacion, $fechaEvaluacion,  $numEncuestas);

            //$pass = $this->generar_clave();
            $clave = "";
            //$clave = $this->danecrypt->encode($pass);
         }
         
        if ($result_evaluacion) {
            $data["result"] = "error";
            $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Ya existe un registro con los valores digitados.');
        } else {
               
                if ($idEvaluacion = $this->planeadas->save_evaluacion($clave)) {
                    $data["result"] = true;                 
                    $this->session->set_flashdata('retornoExito', $msj);
                    
                    //a los usuarios nuevos les envio correo con contraseña
                    /*if($idUser == '') {
                        $this->email($idUsuario);
                    }*/
                } else {
                    $data["result"] = "error";                  
                    $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Contactarse con el administrador.');
                }
        }

        echo json_encode($data);
    }
    
    public function cerrarSesion() {
        $this->load->helper("url");
        $this->load->library("session");
        $this->session->sess_destroy();
        redirect("login", "refresh");
    }

}

//EOC