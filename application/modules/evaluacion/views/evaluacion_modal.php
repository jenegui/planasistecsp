<script type="text/javascript" src="<?php echo base_url("assets/js/validate/evaluacion/evaluacion.js"); ?>"></script>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="exampleModalLabel">Evaluación de percepci&oacute;n de la Asistencia T&eacute;cnica
		<br><small>Adicionar/Editar Evaluaci&oacute;n</small>
	</h4>
</div>

<div class="modal-body">

	<p class="text-danger text-left">Los campos con * son obligatorios.</p>

	<form name="form" id="form" role="form" method="post" >
		<input type="hidden" id="hddId" name="hddId" value="<?php echo $this->session->userdata("idActividad");?>"/>
		<input type="hidden" id="idEvaluacion" name="idEvaluacion" value="<?php echo $information?$information[0]["id_evaluacion"]:"x"; ?>"/>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="evaluacion">Seleccione la evaluaci&oacute;n : *</label>
					<select name="evaluacion" id="evaluacion" class="form-control" required>
						<option value=''>Selecccione...</option>
						<option value=1 <?php if($information[0]["periodo_evaluacion"] == 1) { echo "selected"; }  ?>>1a Evaluación de percepción de la Asistencia Técnica</option>
						<option value=2 <?php if($information[0]["periodo_evaluacion"] == 2) { echo "selected"; }  ?>>2a Evaluación de percepción de la Asistencia Técnica</option>
						<option value=2 <?php if($information[0]["periodo_evaluacion"] == 3) { echo "selected"; }  ?>>3a Evaluación de percepción de la Asistencia Técnica</option>
					</select>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="fechaEvaluacion">Fecha de evaluaci&oacute;n: *</label>
					<input type="text" id="fechaEvaluacion" name="fechaEvaluacion" class=" form-control datepicker" value="<?php echo $information?$information[0]["fecha_evaluacion"]:""; ?>" placeholder="mm/dd/aaaa" required>
				</div>
			</div>

			<div class="col-sm-4 otro">
				<div class="form-group text-left">
					<label class="control-label" for="numEncuestas">No. de encuestas aplicadas:*</label>
					<input type="text" id="numEncuestas" name="numEncuestas" class="form-control" value="<?php echo $information?$information[0]["nro_encuestas"]:""; ?>" placeholder="No. de encuestas aplicadas" required>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="contenidos">Contenidos:*</label>
					<input type="text" id="contenidos" name="contenidos" class="form-control" value="<?php echo $information?$information[0]["contenidos"]:""; ?>" placeholder="Contenidos" required>
				</div>
			</div>

			<div class="col-sm-4 otro">
				<div class="form-group text-left">
					<label class="control-label" for="metodologia">Metodolog&iacute;a:*</label>
					<input type="text" id="metodologia" name="metodologia" class="form-control" value="<?php echo $information?$information[0]["metodologia"]:""; ?>" placeholder="metodologia" required>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="facilitadores">Facilitadores:*</label>
					<input type="text" id="facilitadores" name="facilitadores" class="form-control" value="<?php echo $information?$information[0]["facilitadores"]:""; ?>" placeholder="Quienes impartieron la AT" required>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="general">Calificaci&oacute;n general:*</label>
					<input type="text" id="general" name="general" class="form-control" value="<?php echo $information?$information[0]["general"]:""; ?>" placeholder="Calificación general" required>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="aspectos_positivos">Aspectos positivos:*</label>
					<textarea name="aspectos_positivos" id="aspectos_positivos" class="form-control" rows="2" cols="50" required><?php echo $information?$information[0]["aspectos_positivos"]:""; ?></textarea>
				</div>		
			</div>	
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="aspectos_mejorar">Aspectos a mejorar:*</label>
					<textarea name="aspectos_mejorar" id="aspectos_mejorar" class="form-control" rows="2" cols="50" required><?php echo $information?$information[0]["aspectos_mejorar"]:""; ?></textarea>
				</div>		
			</div>	
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="otros">Otros temas que se consideran necesario brindar en AT:*</label>
					<textarea name="otros" id="otros" class="form-control" rows="2" cols="50" required><?php echo $information?$information[0]["otros"]:""; ?></textarea>
				</div>		
			</div>	
		</div>	
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="soportes">Soportes: *</label>
					<input type="text" id="soportes" name="soportes" class=" form-control" value="<?php echo $information?$information[0]["soportes"]:""; ?>" placeholder="Ruta de almacenamiento" required>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row" align="center">
				<div style="width:50%;" align="center">
					<input type="button" id="btnSubmit" name="btnSubmit" value="Guardar" class="btn btn-primary"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div id="div_load" style="display:none">		
				<div class="progress progress-striped active">
					<div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
						<span class="sr-only">45% completado</span>
					</div>
				</div>
			</div>
			<div id="div_error" style="display:none">			
				<div class="alert alert-danger"><span class="glyphicon glyphicon-remove" id="span_msj">Ya existe un registro con los datos ingresados.</span></div>
			</div>	
		</div>

	</form>
</div>
