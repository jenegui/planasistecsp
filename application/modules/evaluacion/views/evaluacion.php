<?php 
	//Los datos que yo recibo en esta vista se propagan a traves de las demas vistas qye yo llame desde esta vista, 
	//por lo que la variable controller puede ser accedida desde cualquiera de las vistas de los capitulos.
	$this->load->library("general");
	
?>
<div id="content">
	<div class="panel-heading">
		<a class="btn btn-success" href=" <?php echo base_url(). 'planeacion/planeada'; ?> "><span class="glyphicon glyphicon glyphicon glyphicon-backward" aria-hidden="true"></span> Regresar </a> 
	</div>
		<?php
  		foreach ($info as $lista):
		?>
			<div id="page-wrapper">
				<br>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h4 class="list-group-item-heading">
									<i class="fa fa-gear fa-fw"></i> &nbsp;  SECCI&Oacute;N 3. EVALUACI&Oacute;N DE LA ASISTENCIA T&Eacute;CNICA IMPARTIDA
								</h4>
							</div>
						</div>
					</div>
					<!-- /.col-lg-12 -->				
				</div>									
				<!-- /.row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-reorder"></i> LISTA DE EVALUACIONES 
							</div>
							<div class="panel-body">
								<button type="button" class="btn btn-success btn-block evaluacion" data-toggle="modal" data-target="#modal">
									<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Adicionar Evaluaci&oacute;n</button><br>
								<?php
								$retornoExito = $this->session->flashdata('retornoExito');
								if ($retornoExito) {
									?>
									<div class="col-lg-12">	
										<div class="alert alert-success ">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
											<?php echo $retornoExito ?>		
										</div>
									</div>
									<?php
								}

								$retornoError = $this->session->flashdata('retornoError');
								if ($retornoError) {
									?>
									<div class="col-lg-12">	
										<div class="alert alert-danger ">
											<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
											<?php echo $retornoError ?>
										</div>
									</div>
									<?php
								}
								?> 
								<?php
								if($info){
									?>				
									<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
										<thead>
											<tr>
												<th class="text-center">No. Evaluaci&oacute;n</th>
												<th class="text-center">No. de actividad</th>
												<th class="text-center">Editar</th>
												<th class="text-center">Periodo de evaluaci&oacute;n</th>
												<th class="text-center">Fecha de evaluaci&oacute;n </th>
												<th class="text-center">Nro de encuestas</th>
												<th class="text-center">Contenidos</th>
												<th class="text-center">Metodolog&iacute;a </th>
												<th class="text-center">Facilitadores</th>
												<th class="text-center">General</th>
												<th class="text-center">Aspectos Positivos</th>
												<th class="text-center">Aspectos a mejorar</th>
												<th class="text-center">Otros</th>
												<th class="text-center">Soportes</th>
											</tr>
										</thead>
										<tbody>							
											<?php
											foreach ($infoEval as $listaEval):
												//if($listaCron['mes']==$i && $idActividad==$listaCron['id_actividad']){
													$regVacio="";
													echo "<tr>";
														echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;" . $listaEval['id_evaluacion'] . "</td>";
														echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;" . $idActividad . "</td>";
														echo "<td class='text-center'>";
														?>
														<button type="button" class="btn btn-success btn-xs evaluacion" data-toggle="modal" data-target="#modal" id="Ed-<?php echo $listaEval['id_evaluacion']; ?>">
															Editar <span class="glyphicon glyphicon-edit" aria-hidden="true">
															</button>
															<?php
														
															echo "</td>";
															echo "<td class='text-center'>" . $listaEval['periodo_evaluacion'] . "</td>";
															echo "<td class='text-center'>". $listaEval['fecha_evaluacion'] ."</td>";
															echo "<td class='text-center'>". $listaEval['nro_encuestas'] ."</td>";
															echo "<td class='text-center'>" . $listaEval['contenidos'] . "</td>";
															echo "<td class='text-center'>" . $listaEval['metodologia'] . "</td>";
															echo "<td class='text-center'>" . $listaEval['facilitadores'] . "</td>";
															echo "<td class='text-center'>" . $listaEval['general'] . "</td>";
															echo "<td class='text-center'>" . $listaEval['aspectos_positivos'] . "</td>";
															echo "<td class='text-center'>" . $listaEval['aspectos_mejorar'] . "</td>";
															echo "<td class='text-center'>" . $listaEval['otros'] . "</td>";
															echo "<td class='text-center'>" . $listaEval['soportes'] . "</td>";
													echo "</tr>";
												//}
											endforeach;
											
											?>
										</tbody>
									</table>
								<?php } ?>
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
				<!-- /#page-wrapper -->
			<!--INICIO Modal para adicionar HAZARDS -->
			<div class="modal fade text-center" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">    
				<div class="modal-dialog" role="document">
					<div class="modal-content" id="tablaDatos">

					</div>
				</div>
			</div>                       
			<!--FIN Modal para adicionar HAZARDS -->

			<!-- Tables -->	
			<?php
  			
  		endforeach;
  		?>
  	
</div>
		
<!--<li><a href="#tabs-2">Proin dolor</a></li>
				    <li><a href="#tabs-3">Aenean lacinia</a></li>

				 //'. $this->general->obtenerImagen($modulo1["imagen"]).'-->
