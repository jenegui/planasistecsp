<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parametricas extends CI_Model {

    function __construct(){        
        parent::__construct();
        $this->load->database();
        $this->load->library("session");
    }
    
    /**
     * Obtiene la lista de Modalidad de Asistencia Técnica.
     * @author sjneirag
     * @since  01/2020
     */
    function obtenerModalidad(){
    	$modalidad = array();
    	$sql = "SELECT id_modalidad, nombre_modalidad
   		FROM pat_param_modalidad
      WHERE estado_modalidad='A'
   		ORDER BY id_modalidad ASC ";
    	 
    	$query = $this->db->query($sql);
    	
    	if ($query->num_rows()>0){
    		$i=0;
    		foreach($query->result() as $row){
    			$modalidad[$i]["id_modalidad"] = $row->id_modalidad;
          $modalidad[$i]["modalidad"] = $row->nombre_modalidad;
         $i++;
    		}
    	}
    	$this->db->close();
    	return $modalidad;
    }

    /**
     * Obtiene la lista de Ámbito de aplicación.
     * @author sjneirag
     * @since  01/2020
     */
      function obtenerAmbito(){
      $ambito = array();
      $sql = "SELECT id_ambito, nombre_ambito
      FROM pat_param_ambito
      WHERE estado_ambito='A'
      ORDER BY id_ambito ASC ";
       
      $query = $this->db->query($sql);
      
      if ($query->num_rows()>0){
        $i=0;
        foreach($query->result() as $row){
          $ambito[$i]["id_ambito"] = $row->id_ambito;
          $ambito[$i]["ambito"] = $row->nombre_ambito;
         $i++;
        }
      }
      $this->db->close();
      return $ambito;
    }

    /**
     * Obtiene la lista de Área de operación.
     * @author sjneirag
     * @since  01/2020
     */
      function obtenerArea($id_ambito){
      $area = array();
      $sql = "SELECT  id_area, nombre_area, id_ambito_area
      FROM pat_param_area
      WHERE estado_area='A' ";
      if (($id_ambito!=0)&&($id_ambito!="")){
        $sql.= " AND id_ambito_area = $id_ambito ";
      } 
       $sql.= "ORDER BY id_area ASC ";
       
      $query = $this->db->query($sql);
      
      if ($query->num_rows()>0){
        $i=0;
        foreach($query->result() as $row){
          $area[$i]["id_area"] = $row->id_area;
          $area[$i]["area"] = $row->nombre_area ;
         $i++;
        }
      }
      $this->db->close();
      return $area;
    }
    
    /**
     * Obtiene la lista de Tipo de capacidad a desarrollar.
     * @author sjneirag
     * @since  01/2020
     */
      function obtenerCapacidad(){
      $capacidad = array();
      $sql = "SELECT  id_tipo_capacidad, nombre_tipo_capacidad
      FROM pat_param_tipo_capacidad
      WHERE estado_tipo_capacidad='A'
      ORDER BY id_tipo_capacidad ASC ";
       
      $query = $this->db->query($sql);
      
      if ($query->num_rows()>0){
        $i=0;
        foreach($query->result() as $row){
          $capacidad[$i]["id_tipo_capacidad"] = $row->id_tipo_capacidad;
          $capacidad[$i]["tipoCapacidad"] = $row->nombre_tipo_capacidad ;
         $i++;
        }
      }
      $this->db->close();
      return $capacidad;
    }
    
    /**
     * Obtiene la lista de Dependencias.
     * @author sjneirag
     * @since  01/2020
     */
      function obtenerDependencia(){
      $dependencia = array();
      $sql = "SELECT  id_dependencia, nombre_dependencia, estado_dependencia
      FROM pat_param_dependencia
      WHERE estado_dependencia='A' 
      ORDER BY id_dependencia ASC ";
       
      $query = $this->db->query($sql);
      
      if ($query->num_rows()>0){
        $i=0;
        foreach($query->result() as $row){
          $dependencia[$i]["idDependencia"] = $row->id_dependencia;
          $dependencia[$i]["nomDependencia"] = $row->nombre_dependencia ;
         $i++;
        }
      }
      $this->db->close();
      return $dependencia;
    }

    /**
     * Obtiene la lista de meses.
     * @author sjneirag
     * @since  01/2020
     */
    function obtenerMeses(){
      $meses = array();
      $sql = "SELECT  id_mes, nombre_mes, abrev_mes
      FROM pat_param_meses
      ORDER BY id_mes ASC ";
       
      $query = $this->db->query($sql);
      
      if ($query->num_rows()>0){
        $i=0;
        foreach($query->result() as $row){
          $meses[$i]["idMes"] = $row->id_mes;
          $meses[$i]["nomMes"] = $row->nombre_mes ;
          $meses[$i]["abrevMes"] = $row->abrev_mes ;
         $i++;
        }
      }
      $this->db->close();
      return $meses;
    }

    
}//EOC