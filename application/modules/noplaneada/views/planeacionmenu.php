<?php $this->load->library("session");
	  $this->load->helper("url");	
	  $periodo = $this->session->userdata('ano_periodo') . $this->session->userdata('mes_periodo');
         
?>

<div id="menu" class="row">
	<div id="nav">
       <ul>
         <li><a href="<?php echo site_url("planeacion/planeada"); ?>">Planeaci&oacute;n</a></li>
         <li><a href="<?php echo site_url("noplaneada/index"); ?>">No Planeadas</a></li>
         <li><a href="<?php echo site_url("administrador/usuarios"); ?>">Usuarios</a></li>
         <li><a href="<?php echo site_url("runner"); ?>">Reportes</a></li>
         <li><a href="<?php echo site_url("administrador/cerrarSesion"); ?>">Salir</a></li>
       </ul>
     </div>
</div>
