<script type="text/javascript" src="<?php echo base_url("assets/js/validate/noplaneacion/noplaneacion.js"); ?>"></script>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="exampleModalLabel">Formulario de Planeaci&oacute;n e implementaci&oacute;n
		<br><small>Adicionar/Editar Actividad</small>
	</h4>
</div>

<div class="modal-body">

	<p class="text-danger text-left">Los campos con * son obligatorios.</p>

	<form name="form" id="form" role="form" method="post" >
		<input type="hidden" id="hddId" name="hddId" value="<?php echo $information?$information[0]["idActividad"]:"x"; ?>"/>
		
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="modalidad">Modalidad de asistencia técnica *</label>
					<select name="modalidad" id="modalidad" class="form-control" required>
						<option value=''>Selecccione...</option>
						<?php for ($i = 0; $i < count($modalidad); $i++) { ?>
							<option value="<?php echo $modalidad[$i]["id_modalidad"]; ?>" <?php if($information[0]["modalidad"] == $modalidad[$i]["id_modalidad"]) { echo "selected"; }  ?>><?php echo $modalidad[$i]["modalidad"]; ?></option>	
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="ambito">Ámbito de aplicación : *</label>
					<select name="ambito" id="ambito" class="form-control" required>
						<option value=''>Selecccione...</option>
						<?php for ($i = 0; $i < count($ambito); $i++) { ?>
							<option value="<?php echo $ambito[$i]["id_ambito"]; ?>" <?php if($information[0]["ambito"] == $ambito[$i]["id_ambito"]) { echo "selected"; }  ?>><?php echo $ambito[$i]["ambito"]; ?></option>	
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="areaOperacion">Área de operación: *</label>
					<select name="areaOperacion" id="areaOperacion" class="form-control" required>
						<option value=''>Selecccione...</option>
						<?php for ($i = 0; $i < count($area); $i++) { ?>
							<option value="<?php echo $area[$i]["id_area"]; ?>" <?php if($information[0]["area_operacion"] == $area[$i]["id_area"]) { echo "selected"; }  ?>><?php echo $area[$i]["area"]; ?></option>	
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="col-sm-4 otro">
				<div class="form-group text-left">
					<label class="control-label" for="otro">Otro cuál? : *</label>
					<input type="text" id="otroCual" name="otroCual" class="form-control" value="<?php echo $information?$information[0]["cual"]:""; ?>" placeholder="Otro cuál" required disabled>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="personas">Número de personas:</label>
					<input type="text" id="personas" name="personas" class="form-control" value="<?php echo $information?$information[0]["num_personas"]:""; ?>" placeholder="Número de personas" required>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="tipoCapacidad">Tipo de capacidad a desarrollar: *</label>
					<select name="tipoCapacidad" id="tipoCapacidad" class="form-control" required>
						<option value=''>Selecccione...</option>
						<?php for ($i = 0; $i < count($capacidad); $i++) { ?>
							<option value="<?php echo $capacidad[$i]["id_tipo_capacidad"]; ?>" <?php if($information[0]["tip_capacidad"] == $capacidad[$i]["id_tipo_capacidad"]) { echo "selected"; }  ?>><?php echo $capacidad[$i]["tipoCapacidad"]; ?></option>	
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="personas">Tema de la Asistencia Técnicas:</label>
					<textarea name="tema" id="tema" class="form-control" rows="2" cols="50" required><?php echo $information?$information[0]["tema"]:""; ?></textarea>
				</div>		
			</div>
			<div class="col-sm-4">
				<div class="form-group text-left depend">
					<label class="control-label" for="dependencias">Dependencia(s) responsable(s): *</label><br>
					<?php for ($i = 0; $i < count($dependencia); $i++) { 
						$j=$i+1;
						?>
						<input class="dependencia" type="checkbox" class="custom-control-input" id="dependencia<?php echo $dependencia[$i]["idDependencia"]; ?>" name="dependencia<?php echo $dependencia[$i]["idDependencia"]; ?>" value="1" <?php if($information[0]["dependencia_$j"] == 1) { echo "checked"; }  ?>>
						<?php echo $dependencia[$i]["nomDependencia"]; ?><br>
					
					<?php } ?>	
				</div>
				<div id="mensajeDepend"></div>
			</div>	
		</div>	
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="fechaEjecucion">Fecha de ejecuci&oacute;n: *</label>
					<input type="text" id="fechaEjecucion" name="fechaEjecucion" class=" form-control datepicker" value="<?php echo $information?$information[0]["fecha_ejecucion"]:""; ?>" placeholder="mm/dd/aaaa" required>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="numAsistentes">N&uacute;mero de asistentes : *</label>
					<input type="text" id="numAsistentes" name="numAsistentes" class=" form-control" value="<?php echo $information?$information[0]["nro_asistentes"]:""; ?>" placeholder="N&uacute;mero de asistentes" required>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="personas">Facilitadores:</label>
					<textarea name="facilitadores" id="facilitadores" class="form-control" rows="2" cols="50" required><?php echo $information?$information[0]["facilitadores"]:"";?></textarea>
				</div>		
			</div>
			<div class="col-sm-4">
				<div class="form-group text-left">
					<label class="control-label" for="soportes">Soportes: *</label>
					<input type="text" id="soportes" name="soportes" class=" form-control" value="<?php echo $information?$information[0]["soportes"]:""; ?>" placeholder="Ruta de almacenamiento" required>
				</div>
			</div>	
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group text-left">
					<label class="control-label" for="personas">Observaciones:</label>
					<textarea name="observaciones" id="observaciones" class="form-control" rows="2" cols="50" required><?php echo $information?$information[0]["observaciones"]:"";?></textarea>
				</div>		
			</div>	
		</div>
		<div class="form-group">
			<div class="row" align="center">
				<div style="width:50%;" align="center">
					<input type="button" id="btnSubmit" name="btnSubmit" value="Guardar" class="btn btn-primary"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div id="div_load" style="display:none">		
				<div class="progress progress-striped active">
					<div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
						<span class="sr-only">45% completado</span>
					</div>
				</div>
			</div>
			<div id="div_error" style="display:none">			
				<div class="alert alert-danger"><span class="glyphicon glyphicon-remove" id="span_msj">Ya existe un registro con los datos ingresados.</span></div>
			</div>	
		</div>

	</form>
</div>
