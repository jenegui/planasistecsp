<div id="page-wrapper">
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="list-group-item-heading">
						<i class="fa fa-gear fa-fw"></i> &nbsp; SECCI&Oacute;N 1. EJECUCI&Oacute;N
					</h4>
				</div>
			</div>
		</div>
		<!-- /.col-lg-12 -->				
	</div>
	
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-reorder"></i> LISTA DE ACTIVDADES NO PLANEADAS
				</div>
				<div class="panel-body">
					<button type="button" class="btn btn-success btn-block noplaneada" data-toggle="modal" data-target="#modal" name="modalAct" id="x">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Adicionar Actividad
					</button><br>
					<?php
					$retornoExito = $this->session->flashdata('retornoExito');
					if ($retornoExito) {
						?>
						<div class="col-lg-12">	
							<div class="alert alert-success ">
								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								<?php echo $retornoExito ?>		
							</div>
						</div>
						<?php
					}

					$retornoError = $this->session->flashdata('retornoError');
					if ($retornoError) {
						?>
						<div class="col-lg-12">	
							<div class="alert alert-danger ">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<?php echo $retornoError ?>
							</div>
						</div>
						<?php
					}
					?> 
					<?php
					if($info){
						?>				
						<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
							<thead>
								<tr>
									<th class="text-center">No. Actividad</th>
									<th class="text-center">Editar</th>
									<th class="text-center">Modalidad Asistencia T&eacute;cnica</th>
									<th class="text-center">Ambito de aplicaci&oacute;n</th>
									<th class="text-center">&Aacute;rea de operaci&oacute;n</th>
									<th class="text-center">Otra area de operaci&oacute;n</th>
									<th class="text-center">N&uacute;mero de personas </th>
									<th class="text-center">Evaluar</th>
									<th class="text-center">Tipo de capacidad a desarrollar</th>								
									<th class="text-center">Tema de la Asistencia T&eacute;cnica</th>
									<th class="text-center">Dependencia(s) responsable(s)</th>
									<th class="text-center">Fecha de ejecuci&oacute;n</th>
									<th class="text-center">N&uacute;mero de asistentes</th>
									<th class="text-center">Facilitadores</th>
									<th class="text-center">Soportes</th>
									<th class="text-center">Observaciones</th>
									<th class="text-center">Fecha de registro</th>
									<th class="text-center">Estado</th>
								</tr>
							</thead>
							<tbody>							
								<?php
								foreach ($info as $lista):
									$idActividad=$lista['idActividad'];
									$cual=$lista['cual']!=''?$lista['cual']:'-';
									echo "<tr>";
										echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;" . $lista['idActividad'] . "</td>";
										echo "<td class='text-center'>";
										?>
										<button type="button" class="btn btn-success btn-xs actividad" data-toggle="modal" data-target="#modal" id="<?php echo $lista['idActividad']; ?>" >
											Editar <span class="glyphicon glyphicon-edit" aria-hidden="true">
											</button>
											<?php
										echo "</td>";
										echo "<td class='text-center'>" . $lista['nomModalidad'] . "</td>";
										echo "<td class='text-center'>". $lista['nomAmbito'] ."</td>";
										echo "<td class='text-center'>". $lista['nomArea'] ."</td>";
											if($lista['cual'] != '0'){
												echo "<td class='text-center'>" . $cual ."</td>";
											}else{
												echo "<td class='text-center'> </td>";
											}
										echo "<td class='text-center'>" . $lista['num_personas'] . "</td>";
											
										echo "<td class='text-center'>";
											?>
											<a href="<?php echo base_url("evaluacion/evaluar/" . $lista['idActividad']); ?>" class="btn btn-success btn-xs">evaluar <span class="glyphicon glyphicon-pencil" aria-hidden="true"></a>
											<?php
										echo "</td>";
										echo "<td class='text-center'>" . $lista['nombreTipoCapacidad'] . "</td>";
										echo "<td class='text-center'>" . $lista['tema'] . "</td>";
										echo "<td class='text-center'>";
										echo '<p class="text-primary">'; 
											for($i=1; $i<=6; $i++){
												$j=$i-1; 
												//Rescata la lista de dependencias
												if($lista['dependencia_'.$i]==1){
											 		echo "- " .$dependencia[$j]['nomDependencia']."<br>";
											 	}
											}
										echo "</p></td>";
										echo "<td class='text-center'>";
										echo '<p class="text-primary">';
											echo $lista['fecha_ejecucion'];	
										echo "</td>";
										echo "<td class='text-center'>". $lista['nro_asistentes'] ."</td>";
										echo "<td class='text-center'>". $lista['facilitadores'] ."</td>";
										echo "<td class='text-center'>". $lista['soportes'] ."</td>";
										echo "<td class='text-center'>". $lista['observaciones'] ."</td>";
										echo "<td class='text-center'>" . $lista['fechaRegistro'] . "</td>";
										echo "<td class='text-center'>";
											switch ($lista['estado_actividad']) {
												case 0:
												$valor = 'Inactivo';
												$clase = "text-danger";
												break;
												case 1:
												$valor = 'Activo';
												$clase = "text-success";
												break;
												case 2:
												$valor = 'Inactivo';
												$clase = "text-primary";
												break;
											}
										echo '<p class="' . $clase . '"><strong>' . $valor . '</strong></p>';
										echo "</td>";
									echo "</tr>";
									endforeach;
									?>
								</tbody>
							</table>
						<?php } ?>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->


	<!--INICIO Modal para adicionar HAZARDS -->
	<div class="modal fade text-center" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">    
		<div class="modal-dialog" role="document">
			<div class="modal-content" id="tablaDatos">

			</div>
		</div>
	</div>                       
	<!--FIN Modal para adicionar HAZARDS -->

	<!-- Tables -->
	