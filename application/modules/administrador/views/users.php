<script>
	
</script>


<div id="page-wrapper">
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="list-group-item-heading">
						<i class="fa fa-gear fa-fw"></i> CONFIGURACIONES - USUARIOS
					</h4>
				</div>
			</div>
		</div>
		<!-- /.col-lg-12 -->				
	</div>
	
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-users"></i> LISTA DE USUARIOS
				</div>
				<div class="panel-body">
					<button type="button" class="btn btn-success btn-block user" data-toggle="modal" data-target="#modal" id="x">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Adicionar Usuario
					</button><br>
					<?php
					$retornoExito = $this->session->flashdata('retornoExito');
					if ($retornoExito) {
						?>
						<div class="col-lg-12">	
							<div class="alert alert-success ">
								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								<?php echo $retornoExito ?>		
							</div>
						</div>
						<?php
					}

					$retornoError = $this->session->flashdata('retornoError');
					if ($retornoError) {
						?>
						<div class="col-lg-12">	
							<div class="alert alert-danger ">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<?php echo $retornoError ?>
							</div>
						</div>
						<?php
					}
					?> 
					<?php
					if($info){
						?>				
						<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
							<thead>
								<tr>
									<th class="text-center">Nombres Completo</th>
									<th class="text-center">Editar</th>
									<th class="text-center">Número de documento</th>
									<th class="text-center">Rol</th>
									<th class="text-center">Estado</th>
									<th class="text-center">Contraseña</th>								
									<th class="text-center">Teléfono</th>
									<th class="text-center">Celular</th>
									<th class="text-center">Email</th>
									<th class="text-center">Dirección</th>
								</tr>
							</thead>
							<tbody>							
								<?php
								foreach ($info as $lista):
									echo "<tr>";
									echo "<td>&nbsp;" . $lista['nombre'] . " ". $lista['apellido'] . "</td>";
									echo "<td class='text-center'>";
									?>
									<button type="button" class="btn btn-success btn-xs user" data-toggle="modal" data-target="#modal" id="<?php echo $lista['idUsuario']; ?>" >
										Editar <span class="glyphicon glyphicon-edit" aria-hidden="true">
										</button>
										<?php
										echo "</td>";
										echo "<td class='text-center'>" . $lista['identificacion'] . "</td>";

										echo "<td class='text-center'>";
										echo '<p class="text-primary"><strong>' . $lista['rol'] . '</strong></p>';
										echo "</td>";

										echo "<td class='text-center'>";
										switch ($lista['estado']) {
											case 0:
											$valor = 'Usuario nuevo';
											$clase = "text-primary";
											break;
											case 1:
											$valor = 'Activo';
											$clase = "text-success";
											break;
											case 2:
											$valor = 'Inactivo';
											$clase = "text-danger";
											break;
										}
										echo '<p class="' . $clase . '"><strong>' . $valor . '</strong></p>';
										echo "</td>";
										echo "<td class='text-center'>";
										?>
									<!-- 
										Se quita la opcion de resetear la contraseña a 123456
									<a href="<?php echo base_url("admin/resetPassword/" . $lista['idUsuario']); ?>" class="btn btn-default btn-xs">Reset <span class="glyphicon glyphicon-lock" aria-hidden="true"></a> 
									-->
									<a href="<?php echo base_url("administrador/cambiar_password/" . $lista['idUsuario']); ?>" class="btn btn-default btn-xs">Cambiar contraseña <span class="glyphicon glyphicon-lock" aria-hidden="true"></a>

										<?php
										echo "</td>";
										echo "<td class='text-center'>" . $lista['telefono'] . "</td>";
										$movil = $lista["celular"];
										// Separa en grupos de tres 
										$count = strlen($movil); 

										$num_tlf1 = substr($movil, 0, 3); 
										$num_tlf2 = substr($movil, 3, 3); 
										$num_tlf3 = substr($movil, 6, 2); 
										$num_tlf4 = substr($movil, -2); 

										if($count == 10){
											$resultado = "$num_tlf1 $num_tlf2 $num_tlf3 $num_tlf4";  
										}else{

											$resultado = chunk_split($movil,3," "); 
										}

										echo "<td class='text-center'>" . $resultado . "</td>";
										echo "<td>" . $lista['email'] . "</td>";
										echo "<td>" . $lista['direccion_usuario'] . "</td>";
										echo "</tr>";
									endforeach;
									?>
								</tbody>
							</table>
						<?php } ?>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->


	<!--INICIO Modal para adicionar HAZARDS -->
	<div class="modal fade text-center" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">    
		<div class="modal-dialog" role="document">
			<div class="modal-content" id="tablaDatos">

			</div>
		</div>
	</div>                       
	<!--FIN Modal para adicionar HAZARDS -->

	<!-- Tables -->
	<script>
		
	</script>