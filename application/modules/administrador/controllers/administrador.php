<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Administrador extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->helper("download");
        $this->load->library("session");
        $this->load->library("validarsesion");
        $this->load->library("danecrypt");
        $this->load->library("pagination"); //Este es el paginador propio de CodeIgniter        
        $this->load->library('phpexcel/PHPExcel');
    }

    //Funcion principal. Se ejecuta por defecto al cargar el controlador ()
    public function index() {
        
        $data["controller"] = $this->session->userdata("controlador");
        $nom_usuario = $this->session->userdata("nombre");
        $tipo_usuario = $this->session->userdata("tipo_usuario");
        $data['tipo_usuario'] = $this->session->userdata("tipo_usuario");
        $data["nom_usuario"] = $nom_usuario;
        $data["view"] = "administrador";
        $data["menu"] = "adminmenu";
        //$data["periodos"] = $this->periodo->obtenerPeriodosTodosMOD();
        
        $this->load->view("layout", $data);
    }

    public function usuarios()
    {   
        $this->load->model("usuario");
        $data["menu"] = "adminmenu";
        $data["controller"] = $this->session->userdata("controlador");
        $arrParam = array();
        $data['info'] = $this->usuario->get_users($arrParam);
         $data["view"] = 'users';
        $this->load->view("layout", $data);
    }

    /**
     * Cargo modal - formulario Usuarios
     * @since 21/01/2020
     */
    public function cargarModal() 
    {
        header("Content-Type: text/plain; charset=utf-8"); //Para evitar problemas de acentos
        $this->load->model("usuario");
        $data['information'] = FALSE;
        $data["idUser"] = $this->input->post("idRow");

        //$this->load->model("general_model");
        $arrParam = array(
            "table" => "pat_param_roles",
            "order" => "nom_rol",
            "id" => "x"
        );
        $data['roles'] = $this->usuario->get_basic_search($arrParam);         
        $data['tipodocs'] = $this->usuario->obtener_tipdocumetos(); 
        if ($data["idUser"] != 'x') 
        {
            $arrParam = array(
                "idUsuario" => $data["idUser"]
            );
            $data['information'] = $this->usuario->get_users($arrParam);
        }
        
        $this->load->view("user_modal", $data);
    }

    //Método para cambio de contraseña
    public function cambiar_password($idUser){
        $this->load->model("usuario");
        if (empty($idUser)) {
            show_error('ERROR!!! - Estás en el lugar equivocado. Falta el ID USUARIO.');
        }
        $data["controller"] = $this->session->userdata("controlador");
        $data["menu"] = "adminmenu";            
        $arrParam = array(
            "idUsuario" => $idUser
        );
        $data['information'] = $this->usuario->get_users($arrParam);
        
        $data["view"] = "form_password";
        $this->load->view("layout", $data);
    }
    
    public function planeada(){
        $data["controller"] = $this->session->userdata("controlador");
        $data["view"] = "administrador";
        $data["menu"] = "adminmenu";
        $this->load->view("layout", $data);
    }

    /**
     * Update user
     * @since 21/01/2020
     
     */
    public function save_user()
    {           
        header('Content-Type: application/json');
        $this->load->model("usuario");
        $data = array();

        $idUser = $this->input->post('hddId');

        $msj = "Se adicionó un nuevo usuario.";
        if ($idUser != '') {
            $msj = "Se actualizó el usuario con exito.";
        }           

        $documento = $this->input->post('documento');

        $result_user = false;
        $clave = "";
        if ($idUser == '') {
            //Verify if the user already exist by the user name
            $arrParam = array(
                "column" => "num_identificacion",
                "value" => $documento
            );
            $result_user = $this->usuario->verifyUser($arrParam);
            //print_r($result_user)."MMMM";
            $pass = $this->generar_clave();
            //$clave = $this->input->post('documento');
            $clave = $this->danecrypt->encode($pass);
         }

        if ($result_user) {
            $data["result"] = "error";
            $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Este número de documento ya existe en la base de datos.');
        } else {
                if ($idUsuario = $this->usuario->saveUser($clave)) {
                    $data["result"] = true;                 
                    $this->session->set_flashdata('retornoExito', $msj);
                    
                    //a los usuarios nuevos les envio correo con contraseña
                    /*if($idUser == '') {
                        $this->email($idUsuario);
                    }*/
                } else {
                    $data["result"] = "error";                  
                    $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Contactarse con el administrador.');
                }
        }

        echo json_encode($data);
    }

    public function generar_clave()
    {
            $key = "";
            $caracteres = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            
            $length = 10;
            $max = strlen($caracteres) - 1;
            for ($i=0;$i<$length;$i++) {
                $key .= substr($caracteres, rand(0, $max), 1);
            }
            return $key;
    }

    /**
     * Update user´s password
     * @since 10/5/2017
     */
    public function update_password()
    {
        $this->load->model("usuario");
        $data = array();            
        $data["titulo"] = "ACTUALIZAR CONTRASEÑA";
        $data["controller"] = $this->session->userdata("controlador");
         $data["menu"] = "adminmenu";
        $newPassword = $this->input->post("inputPassword");
        $confirm = $this->input->post("inputConfirm");
        $passwd = str_replace(array("<",">","[","]","*","^","-","'","="),"",$newPassword); 
        $password = $this->danecrypt->encode($passwd);
        
        $data['linkBack'] = "administrador/usuarios/";
        $data['titulo'] = "<i class='fa fa-unlock fa-fw'></i>CAMBIAR CONTRASEÑA";
        
        if($newPassword == $confirm)
        {                   
                if ($this->usuario->updatePassword()) {
                    $data["msj"] = "Se actualizó la contraseña.";
                    $data["msj"] .= "<br><strong>Número de documento: </strong>" . $this->input->post("hddUser");
                    $data["msj"] .= "<br><strong>Contraseña: </strong>" . $passwd;
                    $data["clase"] = "alert-success";
                }else{
                    $data["msj"] = "<strong>Error!!!</strong> Contactarse con el administrador.";
                    $data["clase"] = "alert-danger";
                }
        }else{
            //definir mensaje de error
            echo "pailas no son iguales";
        }
                    
        $data["view"] = "template/answer";
        $this->load->view("layout", $data);
    }
    //Cierra la sesion del usuario cuando se da click en la opcion salir del menu	
    public function cerrarSesion() {
        $this->load->helper("url");
        $this->load->library("session");
        $this->session->sess_destroy();
        redirect("login", "refresh");
    }

    
    //Actualiza un combo de Municipios con base en un combo de departamentos
    public function actualizarMunicipios() {
        $this->load->model("divipola");
        $iddepto = $this->input->post("id");
        $municipios = $this->divipola->obtenerMunicipios($iddepto);
        echo '<option value="-" selected="selected">Seleccione</option>';
        for ($i = 0; $i < count($municipios); $i++) {
            echo '<option value="' . $municipios[$i]["codigo"] . '">' . $municipios[$i]["nombre"] . '</option>';
        }
    }
}

//EOC
?>