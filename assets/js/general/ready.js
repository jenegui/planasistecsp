var base_url = "/planasistecsppr/";  //Ruta base para ejecutar AJAX en CodeIgniter
  
//*****************************************************************************************
	//* Ejecuta una funcion ajax para actualizar un comboBox
	//*****************************************************************************************
	$.fn.cargarCombo = function(element,url){
		return this.change(function(event){
			$.ajax({
				type: "POST",
				url: base_url + url,
				data: "id=" + $(this).val(),
			    dataType: "html",
				cache: false,
				success: function(html){
					var target = "#" + element;
					$(target).html("");
					$(html).appendTo(target);									
				}
			});
		});
	};

//*****************************************************************************************
	//* End
//*****************************************************************************************	

function toggleNavigation(){
      $('.page-header').toggleClass('menu-expanded');
      $('.page-nav').toggleClass('collapse');
    }
 
    // EVENTOS DEL DOM
    $(window).on('load',function(){
      $('.toggle-nav').click(toggleNavigation);
    });

$("#btnIngresar").click(function () {
    $("#frmIngresar").validate({
        rules: {
            txtLogin: {required: true},
            txtPassword: {required: true}
        },
        messages: {
            txtLogin: {required: "Debe ingresar usuario."},
            txtPassword: {required: "Debe ingresar password."},
        },
        errorPlacement: function (error, element) {
            element.after(error);
            error.css('opacity','0.47');
            error.css('z-index','991');
            error.css('background','#ee0101');
            //error.css('float','right');
            error.css('position','relative');
            error.css('margin-top','1px');
            error.css('color','#fff');
            error.css('font-size','11px');
            error.css('border','2px solid #ddd');
            error.css('box-shadow','0 0 6px #000');
            error.css('-moz-box-shadow','0 0 6px #000');
            error.css('-webkit-box-shadow','0 0 6px #000');
            error.css('padding','4px 10px 4px 10px');
            error.css('border-radius','6px');
            error.css('-moz-border-radius','6px');
            error.css('-webkit-border-radius','6px');
        },
        submitHandler: function (form) {
            form.submit();
            /*$.ajax({
                type: "POST",
                url: base_url + "login/validar",
                data: $("#frmIngresar").serialize(),
                dataType: "html",
                cache: false,
                success: function (data) {
                    //alert(url);
                    $("#agregarEmpresa").dialog('close');
                }
            });*/
        }
    });
});

$(function(){ 
    $(".user").click(function () { 
        var oID = $(this).attr("id");
        $.ajax ({
            type: 'POST',
            url: base_url +"cargarModal",
            data: {'idRow': oID},
            cache: false,
            success: function (data) {
                //  alert(url);
                $('#tablaDatos').html(data);
            }
        });
    }); 
});

$(function(){ 
	$(".actividad").click(function () {	
		var oID = $(this).attr("id");
        $.ajax ({
			type: 'POST',
			url: base_url +"cargarModal",
			data: {'idRow': oID},
			cache: false,
			success: function (data) {
                //  alert(url);
				$('#tablaDatos').html(data);
			}
		});
	});	
});

$(function(){ 
    $(".noplaneada").click(function () { 
        var oID = $(this).attr("id");
        $.ajax ({
            type: 'POST',
            url: base_url +"cargarModal",
            data: {'idRow': oID},
            cache: false,
            success: function (data) {
                //  alert(url);
                $('#tablaDatos').html(data);
            }
        });
    }); 
});
$(function(){ 
    var base_url = "/planasistecsp/";
    $(".evaluacion").click(function () { 
        var oID = $(this).attr("id");
        $.ajax ({
            type: 'POST',
            url: base_url +"evaluacion/cargarModal",
            data: {'idRow': oID},
            cache: false,
            success: function (data) {
                //  alert(url);
                $('#tablaDatos').html(data);
            }
        });
    }); 
});

$(function(){
    $(".mes1,.mes2,.mes3,.mes4,.mes5,.mes6,.mes7,.mes8,.mes9,.mes10,.mes11,.mes12").click(function () {
        var oID = $(this).attr("id");
        var tablaDatos="";
        if(jQuery(this).hasClass('mes1')){
           tablaDatos =$('#tablaDatos1');
        }
        if(jQuery(this).hasClass('mes2')){
            tablaDatos = $('#tablaDatos2');
        }
        if(jQuery(this).hasClass('mes3')){
            tablaDatos = $('#tablaDatos3');
        }
        if(jQuery(this).hasClass('mes4')){
            tablaDatos = $('#tablaDatos4');
        }
        if(jQuery(this).hasClass('mes5')){
            tablaDatos = $('#tablaDatos5');
        }
        if(jQuery(this).hasClass('mes6')){
            tablaDatos = $('#tablaDatos6');
        }
        if(jQuery(this).hasClass('mes7')){
            tablaDatos = $('#tablaDatos7');
        }
        if(jQuery(this).hasClass('mes8')){
            tablaDatos = $('#tablaDatos8');
        }
        if(jQuery(this).hasClass('mes9')){
            tablaDatos = $('#tablaDatos9');
        }
        if(jQuery(this).hasClass('mes10')){
            tablaDatos = $('#tablaDatos10');
        }
        if(jQuery(this).hasClass('mes11')){
            tablaDatos = $('#tablaDatos11');
        }
        if(jQuery(this).hasClass('mes12')){
            tablaDatos = $('#tablaDatos12');
        }
        $.ajax ({
            type: 'POST',
            url: base_url +"../cargarModal",
            data: {'idRow': oID},
            cache: false,
            success: function (data) {
                tablaDatos.html(data);
            }
        });
    });
});


$(document).ready(function() {
	$('#dataTables').DataTable({
		  language: {
            processing:     "Procesando...",
            search:         "Buscar:",
            lengthMenu:    "Mostrar _MENU_ registros",
            info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando registros 0 a 0 de 0 registros",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "No hay registros",
            paginate: {
                first:      "Primera",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Última"
            },
            buttons: {
                print:      "Imprimir",
                copy:       "Copiar",
                copyTitle: "Copiar al portapapeles"
                
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
		responsive: true,
		"order": [[ 1, "DESC" ]],
		"pageLength": 25
	});

    $('#dataTables1, #dataTables2, #dataTables3, #dataTables4, #dataTables5, #dataTables6, #dataTables7, #dataTables8, #dataTables9, #dataTables10, #dataTables11, #dataTables12').DataTable({
          language: {
            processing:     "Procesando...",
            search:         "Buscar:",
            lengthMenu:    "Mostrar _MENU_ registros",
            info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            infoEmpty:      "Mostrando registros 0 a 0 de 0 registros",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Cargando...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "No hay registros para este mes",
            paginate: {
                first:      "Primera",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Última"
            },
            buttons: {
                print:      "Imprimir",
                copy:       "Copiar",
                copyTitle: "Copiar al portapapeles"
                
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        responsive: true,
        "order": [[ 1, "DESC" ]],
        "pageLength": 25
    });
    
});